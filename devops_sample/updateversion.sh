#!/bin/bash

commit_date=$(git log -1 --format=%cd --date=format:"%y%m%d")

commit_hash=$(git log -1 --format=%H | cut -c 1-8)

new_version="${commit_date}-${commit_hash}"

mvn versions:set -DnewVersion=${new_version}

