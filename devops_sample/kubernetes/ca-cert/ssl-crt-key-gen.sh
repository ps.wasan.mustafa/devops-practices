#! /bin/bash

GREEN="\e[1;32m"
RED="\e[1;31m"
COLEND="\e[39m"

if [ "$#" -ne 1 ]
then
  echo "${RED}Error: No domain name argument provided${COLEND}"
  echo "Usage: Provide a domain name as an argument"
  exit 1
fi

DOMAIN=$1
echo "${GREEN}Your domain name is '${DOMAIN}'${COLEND}"

# Delete Existing Files

#rm -rf *.conf *.crt *.csr *.key

# Generate Private key 

openssl genrsa -out myapp.local.key 2048

# Create csf conf

cat > csr.conf <<EOF
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C = JO
ST = Amman
L = Um Uthainah
O = Progressoft
OU = PS
CN = *.local

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = *.local 
DNS.1 = myapp-spring-main.local
DNS.3 = myapp-spring-*.local
DNS.4 = myapp.local

EOF

# create CSR request using private key

openssl req -new -key myapp.local.key -out myapp.local.csr -config csr.conf

# Create a external config file for the certificate

cat > cert.conf <<EOF

authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = *.local
DNS.2 = myapp-spring-main.local
DNS.3 = myapp-spring-*.local
DNS.4 = myapp.local

EOF

# Create SSl with self signed CA

openssl x509 -req \
    -in myapp.local.csr \
    -CA  ~/Desktop/myCA/rootCA.crt -CAkey ~/Desktop/myCA/rootCA.key \
    -CAcreateserial -out myapp.local.crt \
    -days 365 \
    -sha256 -extfile cert.conf
